import java.io.File;
import java.util.*;
import org.example.Alert;

/**
 * This test class is a copy of the Main class, but uses
 * HappyTestPath2.txt. This file is the same as HappyTestPath.txt,
 * except the IDs are switched, so we should see 1001 as the ID
 * being printed out. This is to test the other two parts of the
 * verification code.
 */
public class SatelliteTwoTest {

    // sets the constant to be the first satellite's ID
    private static final int SATELLITE_ID_1 = 1000;

    // sets the constant to be the second satellite's ID
    private static final int SATELLITE_ID_2 = 1001;

    // Scanner object to read in the contents of the satellite
    // status log file
    public static Scanner testScanner = null;

    /**
     * Method to read in all of the alerts and process them to
     * see which alerts are considered "bad" (exceeding the red high limit
     * or below the red low limit)
     * @param args
     */
    public static void main(String[] args) {

        try {
            File testFile = new File("HappyPathTest2.txt");
            testScanner = new Scanner(testFile);
            List<Alert> alerts = new ArrayList<Alert>();

            // read in the file line by line
            // split up each line and use all the arguments
            // as parameters for an Alert object
            // I am assuming that each line has eight parts, and each part
            // is at the same place in each line.
            while (testScanner.hasNextLine()) {
                String[] line = testScanner.nextLine().split("\\|");
                Alert alert1 = new Alert(
                        line[0],
                        Integer.parseInt(line[1]),
                        Integer.parseInt(line[2]),
                        Integer.parseInt(line[3]),
                        Integer.parseInt(line[4]),
                        Integer.parseInt(line[5]),
                        Double.parseDouble(line[6]),
                        line[7]);
                alerts.add(alert1);
            }
            testScanner.close();

            // call upon the method to process the alerts
            processAlerts(alerts);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("File cannot be read.");
        }
    }

    /**
     * Uses 4 different lists to keep track of different types
     * of alerts. I am assuming we only have two different satellites.
     * Each list corresponds to the type of alerts for each component
     * in the satellite (battery and thermostat). I am assuming that
     * each alert is added in chronological order, as the satellite
     * log file should be in chronological order as well.
     *
     * @param alerts - List of alerts to be processes
     */
    public static void processAlerts(List<Alert> alerts) {
        // creates the list to keep track of battery alerts for Satellite 1
        List<Alert> sat1_batt_alerts = new ArrayList<>();

        // creates the list to keep track of thermostat alerts for Satellite 1
        List<Alert> sat1_tstat_alerts = new ArrayList<>();

        // creates the list to keep track of battery alerts for Satellite 2
        List<Alert> sat2_batt_alerts = new ArrayList<>();

        // creates the list to keep track of thermostat alerts for Satellite 2
        List<Alert> sat2_tstat_alerts = new ArrayList<>();

        // go through all the alerts and keep track of the alerts where
        // the raw value exceeds red high limit
        for(Alert alert: alerts){
            // gets all the alerts where the raw value exceeds the red high limit for Satellite 1
            if ((alert.getSatelliteID() == SATELLITE_ID_1) && alert.getRedHighLimit() < alert.getRawValue()) {
                // puts all the bad alerts for the battery in one list
                if(alert.getComponent().contains("BATT")) {
                    sat1_batt_alerts.add(alert);
                }
                // puts all the bad alerts for the thermostat in one list
                else if (alert.getComponent().contains("TSTAT")) {
                    sat1_tstat_alerts.add(alert);
                }
            }
            // gets all the alerts where the raw value exceeds the red high limit for Satellite 2
            else if ((alert.getSatelliteID() == SATELLITE_ID_2) && alert.getRedHighLimit() < alert.getRawValue()){
                // if the component involved is the battery, add the alert to the battery list
                if(alert.getComponent().contains("BATT")){
                    sat2_batt_alerts.add(alert);
                }
                // if the component involved is the thermostat, add the alert to the thermostat list
                else if (alert.getComponent().contains("TSTAT")){
                    sat2_tstat_alerts.add(alert);
                }
            }
        }

        // go through all the alerts and keep track of all the alerts where
        // the red low limit is greater than the raw value
        // This is done separately from the Red High limit, so in the list,
        // the different types of alerts are separated by component and
        // type of error (red high or red low) in chronological order
        for(Alert alert: alerts){
            // gets all the alerts where the raw value is lower than the red low limit for Satellite 1
            if ((alert.getSatelliteID() == SATELLITE_ID_1) && alert.getRedLowLimit() > alert.getRawValue()) {
                // if the component involved is the battery, add the alert to the battery list
                if(alert.getComponent().contains("BATT")) {
                    sat1_batt_alerts.add(alert);
                }
                // if the component involved is the thermostat, add the alert to the thermostat list
                else if (alert.getComponent().contains("TSTAT")) {
                    sat1_tstat_alerts.add(alert);
                }
            }
            // gets all the alerts where the raw value is lower than the red low limit for Satellite 2
            else if ((alert.getSatelliteID() == SATELLITE_ID_2) && alert.getRedLowLimit() > alert.getRawValue()){
                // if the component involved is the battery, add it to the battery list
                if(alert.getComponent().contains("BATT")){
                    sat2_batt_alerts.add(alert);
                }
                // if the component involved is the thermostat, add it to the thermostat list
                else if (alert.getComponent().contains("TSTAT")){
                    sat2_tstat_alerts.add(alert);
                }
            }
        }

        //kept throwing a NullPointerException when parsing through the timestamps
        // Purpose: to compare the timestamps to see if the three times
        // are within five minutes of each other. If so, then record the first
        // timestamp, severity, component and satellit ID.

        /*
        //checking the timestamps to see if they are within 5 minutes of each other in groups of three
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");

        // minutes of the first timestamp
        int minute1 = 0;
        //minutes of the second timestamp
        int minute2 = 0;
        //minutes of the third timestamp
        int minute3 = 0;

        // hour of the first timestamp
        int hour1 = 0;
        // hour of the second timestamp
        int hour2 = 0;
        // hour of the third timestamp
        int hour3 = 0;

        // timestamp of when the first "Red Low" or "Red High" alert occurred
        String initialTime = null;

        //if there are at least 3 alerts to compare; otherwise do not worry about the alert(s)
        if(sat1_batt_alerts.size() >= 3) {
            Calendar calendar = null;

            for (int i = 0; i < sat1_batt_alerts.size() - 2; i++) {
                try{
                    while(i+2 < sat1_batt_alerts.size()) {
                        //format the first timestamp and set the minutes and hours
                        calendar.setTime(sdf.parse(sat1_batt_alerts.get(i).getTimestamp()));
                        //gets the initial timestamp
                        initialTime = sat1_batt_alerts.get(i).getTimestamp();
                        hour1 = calendar.get(Calendar.HOUR_OF_DAY);
                        minute1 = calendar.get(Calendar.MINUTE);

                        //format the second timestamp and set the minutes and hours
                        calendar.setTime(sdf.parse(sat1_batt_alerts.get(i + 1).getTimestamp()));
                        hour2 = calendar.get(Calendar.HOUR_OF_DAY);
                        minute2 = calendar.get(Calendar.MINUTE);

                        //format the third timestamp and set the minutes and hours
                        calendar.setTime(sdf.parse(sat1_batt_alerts.get(i + 2).getTimestamp()));
                        hour3 = calendar.get(Calendar.HOUR_OF_DAY);
                        minute3 = calendar.get(Calendar.MINUTE);

                        //if they are within five minutes of each other and the hours are either
                        //the same or progressing forward, then it's an alert
                        if ((minute3 - minute1 <= 5 && minute2 - minute1 <= 5) && (hour3 >= hour1 && hour2 >= hour1)) {
                            System.out.println(initialTime);
                        }
                    }
                }
                catch (ParseException e){
                    e.printStackTrace();
                    System.out.println("String could not be parsed.");
                }
                catch(NullPointerException e){
                    e.printStackTrace();
                    System.out.println("Something is null. Please check your timestamps");
                }
            }
        }
        */

        //creates a mapping for the output
        List<StringBuilder> alertList = new ArrayList<>();
        StringBuilder sb = new StringBuilder();

        // if there are at least 3 alerts for the battery component for satellite 1
        if(sat1_batt_alerts.size() >= 3) {
            //prints the satellite ID
            sb.append("satelliteID : " + Integer.toString(sat1_batt_alerts.get(0).getSatelliteID()));

            //prints the severity message
            if(sat1_batt_alerts.get(0).getRawValue() > sat1_batt_alerts.get(0).getRedHighLimit()){
                sb.append("\nseverity : RED HIGH");
            } else {
                sb.append("\nseverity : RED LOW");
            }

            //prints the component
            sb.append("\ncomponent: " + sat1_batt_alerts.get(0).getComponent());

            //prints the initial timestamp
            sb.append("\ntimestamp : "+ sat1_batt_alerts.get(0).getTimestamp());
        }
        alertList.add(sb);

        // if there are at least three alerts for the thermostat for Satellite 1
        sb = new StringBuilder();
        if(sat1_tstat_alerts.size() >= 3) {
            //prints the satellite ID
            sb.append("\n\nsatelliteID : " + Integer.toString(sat1_tstat_alerts.get(0).getSatelliteID()));

            //prints the severity message
            if(sat1_tstat_alerts.get(0).getRawValue() > sat1_tstat_alerts.get(0).getRedHighLimit()){
                sb.append("\nseverity : RED HIGH");
            } else {
                sb.append("\nseverity : RED LOW");
            }

            //prints the component
            sb.append("\ncomponent : " + sat1_tstat_alerts.get(0).getComponent());

            //prints the initial timestamp
            sb.append("\ntimestamp : " + sat1_tstat_alerts.get(0).getTimestamp());
        }
        alertList.add(sb);

        sb = new StringBuilder();
        // if there are at least 3 alerts for the battery for Satellite 2
        if(sat2_batt_alerts.size() >= 3) {
            //prints the satellite ID
            sb.append("\n\nsatelliteID : " + Integer.toString(sat2_batt_alerts.get(0).getSatelliteID()));

            //prints the severity message
            if(sat2_batt_alerts.get(0).getRawValue() > sat2_batt_alerts.get(0).getRedHighLimit()){
                sb.append("\nseverity : RED HIGH");
            } else {
                sb.append("\nseverity : RED LOW");
            }

            //prints the component
            sb.append("\ncomponent : " + sat2_batt_alerts.get(0).getComponent());

            //prints the initial timestamp
            sb.append("\ntimestamp : " + sat2_batt_alerts.get(0).getTimestamp());
        }
        alertList.add(sb);

        sb = new StringBuilder();
        //if there are at least alerts for the thermostat for Satellite 2
        if(sat2_tstat_alerts.size() >= 3) {
            //prints the satellite ID
            sb.append("\n\nsatelliteID : " + Integer.toString(sat2_tstat_alerts.get(0).getSatelliteID()));

            //prints the severity message
            if(sat2_tstat_alerts.get(0).getRawValue() > sat2_tstat_alerts.get(0).getRedHighLimit()){
                sb.append("\nseverity : RED HIGH");
            } else {
                sb.append("\nseverity : RED LOW");
            }

            //prints the component
            sb.append("\ncomponent : " + sat2_tstat_alerts.get(0).getComponent());

            //prints the initial timestamp
            sb.append("\ntimestamp : " + sat2_tstat_alerts.get(0).getTimestamp());
        }
        alertList.add(sb);

        for(StringBuilder sb1: alertList){
            System.out.print(sb1);
        }
    }
}