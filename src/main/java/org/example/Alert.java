package org.example;

public class Alert {
    private String timestamp;
    private int satelliteID;
    private int redHighLimit;
    private int yellowHighLimit;
    private int yellowLowLimit;
    private int redLowLimit;
    private double rawValue;
    private String component;

    public Alert(String timestamp, int satelliteID, int redHighLimit, int yellowHighLimit, int yellowLowLimit, int redLowLimit, double rawValue, String component) {
        this.timestamp = timestamp;
        this.satelliteID = satelliteID;
        this.redHighLimit = redHighLimit;
        this.yellowHighLimit = yellowHighLimit;
        this.yellowLowLimit = yellowLowLimit;
        this.redLowLimit = redLowLimit;
        this.rawValue = rawValue;
        this.component = component;
    }
    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public int getSatelliteID() {
        return satelliteID;
    }

    public void setSatelliteID(int satelliteID) {
        this.satelliteID = satelliteID;
    }

    public int getRedHighLimit() {
        return redHighLimit;
    }

    public void setRedHighLimit(int redHighLimit) {
        this.redHighLimit = redHighLimit;
    }

    public int getYellowHighLimit() {
        return yellowHighLimit;
    }

    public void setYellowHighLimit(int yellowHighLimit) {
        this.yellowHighLimit = yellowHighLimit;
    }

    public int getRedLowLimit() {
        return redLowLimit;
    }

    public void setRedLowLimit(int redLowLimit) {
        this.redLowLimit = redLowLimit;
    }

    public int getYellowLowLimit() {
        return yellowLowLimit;
    }

    public void setYellowLowLimit(int yellowLowLimit) {
        this.yellowLowLimit = yellowLowLimit;
    }

    public double getRawValue() {
        return rawValue;
    }

    public void setRawValue(double rawValue) {
        this.rawValue = rawValue;
    }

    public String getComponent() {
        return component;
    }

    public void setComponent(String component) {
        this.component = component;
    }

    @Override
    public String toString() {
        return "Alert{" +
                "timestamp=" + timestamp +
                ", id=" + satelliteID +
                ", redHighLimit=" + redHighLimit +
                ", yellowHighLimit=" + yellowHighLimit +
                ", yellowLowLimit=" + yellowLowLimit +
                ", redLowLimit=" + redLowLimit +
                ", rawValue=" + rawValue +
                ", component='" + component + '\'' +
                '}';
    }
}
